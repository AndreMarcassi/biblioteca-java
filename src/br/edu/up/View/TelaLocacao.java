package br.edu.up.View;

import java.time.LocalDate;

import br.edu.up.Controlers.ControleLivro;
import br.edu.up.Controlers.ControleLocacao;
import br.edu.up.Controlers.ControleUsuario;
import br.edu.up.Models.Locacao;
import br.edu.up.Utils.Prompt;

public class TelaLocacao {

    private ControleLocacao ControleLocacao = new ControleLocacao();
    private ControleLivro ControleLivro = new ControleLivro();
    private ControleUsuario controleUsuario = new ControleUsuario();

    public void LocarLivro(){
        
        Prompt.imprimir(ControleLivro.listarLivros());

        Prompt.separador();

        Prompt.imprimir(controleUsuario.listarUsuarioCpf());

        Prompt.separador();

        int locacaoId = ControleLocacao.getId();

        int idLivro = Prompt.lerInteiro("Digite o ID do livro");
        String cpf = Prompt.lerLinha("Digite o cpf do usuario");

        LocalDate dataLocacao = LocalDate.now();
        LocalDate dataDevolucao = dataLocacao.plusDays(15);

        Locacao locacao = new Locacao(locacaoId, cpf, idLivro, dataLocacao, dataDevolucao);

        Prompt.separador();

        Prompt.imprimir(ControleLocacao.addLocacao(locacao));

    }

    public void DevolverLivro(){
        
        Prompt.imprimir(ControleLocacao.listarLocacoes());

        int idLocacao = Prompt.lerInteiro("Digite o ID da locação");

        Prompt.imprimir(ControleLocacao.LivroDevolver(idLocacao));

    }

    public void ListarLocacoes(){

        Prompt.imprimir(ControleLocacao.listarLocacoes());

    }


}
