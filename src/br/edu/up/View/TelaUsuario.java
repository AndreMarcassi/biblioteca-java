package br.edu.up.View;

import br.edu.up.Controlers.ControleUsuario;
import br.edu.up.Models.Usuario;
import br.edu.up.Utils.Prompt;
import br.edu.up.Utils.Verificadores;

public class TelaUsuario {

    ControleUsuario ControleUsuario = new ControleUsuario();

    private String var;

    public TelaUsuario(){
    }

    public void UsuarioCadastrar(){

        do {

            Prompt.separador();

            String nome = Prompt.lerLinha("Digite o nome do Usuario");
            String cpf = Prompt.lerLinha("Digite o cpf do Usuario");
            String telefone = Prompt.lerLinha("Digite o telefone do Usuario");
            String email = Prompt.lerLinha("Digite o email do Usuario");
            String endereco = Prompt.lerLinha("Digite o endereco do Usuario");

            Usuario usuario = new Usuario(nome, cpf, telefone, email, endereco);

            Prompt.separador();
            Prompt.imprimir(ControleUsuario.addUsuario(usuario));

            Prompt.pularLinha();
            var = Prompt.lerLinha("Deseja cadastrar outro usuario?");
            
        } while (Verificadores.TF(var));

    }

    public void UsuarioListar(){

        Prompt.imprimir(ControleUsuario.listarUsuario());

    }

    public void UsuarioDeletar(){

        String cpf = Prompt.lerLinha("Digite o CPF do usuario que deseja deletar");

        Prompt.imprimir(ControleUsuario.DeletarUsuario(cpf));

    }
    
}