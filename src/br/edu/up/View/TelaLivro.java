package br.edu.up.View;

import br.edu.up.Controlers.ControleLivro;
import br.edu.up.Models.Livro;
import br.edu.up.Utils.Prompt;


public class TelaLivro {
    private ControleLivro ControleLivro = new ControleLivro();

    public TelaLivro(){
    }

    public void LivroCadastrar(){

        Prompt.imprimir(ControleLivro.mostrarCategorias());

        Prompt.separador();

        int livroId = ControleLivro.getId();

        String titulo = Prompt.lerLinha("Digite o titulo do livro");
        String editora = Prompt.lerLinha("Digite a editora do livro");
        String autor = Prompt.lerLinha("Digite o autor do livro");
        int quantidade = Prompt.lerInteiro("Digite a quantidade de livros");
        int idCategoria = Prompt.lerInteiro("Digite a categoria do livros");

        Livro livro = new Livro(livroId, titulo, editora, autor, quantidade, idCategoria);

        Prompt.separador();

        Prompt.imprimir(ControleLivro.addLivro(livro));

    }

    public void LivroListar(){

        Prompt.imprimir(ControleLivro.listarLivros());

    }

    public void LivroDeletar(){

        String titulo = Prompt.lerLinha("Digite o titulo do livro que deseja deletar");

        Prompt.imprimir(ControleLivro.DeletarLivro(titulo));

    }
    
}
