package br.edu.up.View;

import br.edu.up.Controlers.ControleCategoria;
import br.edu.up.Models.Categoria;
import br.edu.up.Utils.Prompt;
import br.edu.up.Utils.Verificadores;

public class TelaCategoria {

    ControleCategoria ControleCategoria = new ControleCategoria();

    private String var;

    public TelaCategoria(){
    }

    public void CategoriaCadastrar(){

        do {

            Prompt.pularLinha();

            int categoriaId = ControleCategoria.getId();

            String categoria = Prompt.lerLinha("Digite a categoria que deseja cadastrar.");

            Categoria nova = new Categoria(categoriaId, categoria.toLowerCase());

            Prompt.pularLinha();
            Prompt.imprimir(ControleCategoria.addCategoria(nova));
            Prompt.pularLinha();
            var = Prompt.lerLinha("Deseja cadastrar outra categoria?");
            
        } while (Verificadores.TF(var));

        Prompt.pularLinha();

    }

    public void CategoriaListar(){

        Prompt.separador();

        Prompt.imprimir("              Categorias");

        Prompt.imprimir(ControleCategoria.listarCategorias());

    }
    
}