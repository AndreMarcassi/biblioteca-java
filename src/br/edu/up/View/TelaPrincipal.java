package br.edu.up.View;
import br.edu.up.Utils.Prompt;

public class TelaPrincipal {

    TelaCategoria categoria = new TelaCategoria();
    TelaUsuario usuario = new TelaUsuario();
    TelaLivro livro = new TelaLivro();
    TelaLocacao locacao = new TelaLocacao();

    public void Menu(){

        Prompt.separador();

        Prompt.imprimir("              ESCOLHA UMA DAS OPÇÕES");
    
        Prompt.separador();

        Prompt.imprimir("1.  Cadastrar Categoria");
        Prompt.imprimir("2.  Listar Categoria");

        Prompt.separador();

        Prompt.imprimir("3.  Cadastrar Usuario");
        Prompt.imprimir("4.  Listar Usuarios");
        Prompt.imprimir("5.  Deletar Usuario");

        Prompt.separador();

        Prompt.imprimir("6.  Cadastrar Livro");
        Prompt.imprimir("7.  Listar Livros");
        Prompt.imprimir("8.  Deletar Livro");

        Prompt.separador();

        Prompt.imprimir("9.  Efetuar Locação");
        Prompt.imprimir("10. Efetuar Devolução");
        Prompt.imprimir("11. listar Locações");

        Prompt.separador();

        int num = Prompt.lerInteiro("");

        MainSwitch(num);
        
    }

    public void Retorno(){

        Prompt.separador();

        Prompt.imprimir("              ESCOLHA UMA DAS OPÇÕES");
    
        Prompt.separador();

        Prompt.imprimir("1.  Voltar ao menu principal");
        Prompt.imprimir("2.  Finalizar Programa");

        Prompt.separador();

        int num = Prompt.lerInteiro("");

        ReturnSwitch(num);

    }

    public void MainSwitch(int var){

        switch (var) {
            case 1:
                categoria.CategoriaCadastrar();
                break;

            case 2:
                categoria.CategoriaListar();
                break;

            case 3:
                usuario.UsuarioCadastrar();
                break;

            case 4:
                usuario.UsuarioListar();
                break;

            case 5:
                usuario.UsuarioDeletar();
                break;

            case 6:
                livro.LivroCadastrar();
                break;

            case 7:
                livro.LivroListar();
                break;

            case 8:
                livro.LivroDeletar();
                break;

            case 9:
                locacao.LocarLivro();
                break;

            case 10:
                locacao.DevolverLivro();
                break;

            case 11:
                locacao.ListarLocacoes();
                break;

            default:
                break;

        }

        Retorno();

    }
    
    public void ReturnSwitch(int var){

        switch (var) {
            case 1:
                Menu();
                break;
        
            default:
                break;
        }

    }
    
}