package br.edu.up.Controlers;

import java.util.ArrayList;
import java.util.List;

import br.edu.up.Daos.GerenciadorDeLivros;
import br.edu.up.Models.Livro;

public class ControleLivro {

    private List<Livro> livros = new ArrayList<>();
    private ControleCategoria ControleCategoria = new ControleCategoria();
    private GerenciadorDeLivros daoLivro = new GerenciadorDeLivros();
    private int maiorId = 0;

    public ControleLivro() {

        livros = daoLivro.getLivros();

        for (Livro livro : this.livros) {
            
            if (livro.getLivroId() > maiorId) {
                maiorId = livro.getLivroId();
            }

        }

    }

    public int getId() {
        return ++maiorId;
    }

    public String addLivro(Livro livro) {

        for (Livro livroVeri : livros) {

            if (livroVeri.getTitulo().equals(livro.getTitulo())) {
                livro.setQuantidade(+livro.getQuantidade());
                return "livro já cadastrado, adicionado a quantidade";
            }

        }

        livros.add(livro);
        daoLivro.gravarArquivo();
        return "Livro cadastrado com sucesso";

    }

    //----------------------------------------------------------------------------------------------------------

    public String mostrarCategorias(){
        return ControleCategoria.listarCategorias();
    }

    public String mostrarNome(int idLivro){

        for (Livro livro : livros) {

            if (livro.getLivroId()==idLivro) {
                return livro.getTitulo();  
            }

        }

        return "";

    }

    //-----------------------------------------------------------------------------------------------------------

    public String listarLivros() {

        StringBuilder listarLivros = new StringBuilder();

        if (livros.isEmpty()) {
            return "Nenhum livro cadastrado";
        }

        for (Livro livro : livros) {

            listarLivros.append("ID: ")
                        .append(livro.getLivroId())
                        .append(", Nome: ")
                        .append(livro.getTitulo())
                        .append(", Editora: ")
                        .append(livro.getEditora())
                        .append(", Autor: ")
                        .append(livro.getAutor())
                        .append(", Categoria: ")
                        .append(ControleCategoria.mostrarUmaCategoria(livro.getIdCategoria()))
                        .append("\n");

        }

        return listarLivros.toString();

    }

    //-------------------------------------------------------------------------------------------------

    public String DeletarLivro(String titulo) {

        if (livros.isEmpty()) {
            return "Nenhum livro cadastrado";
        }

        for (Livro livro : livros) {

            if (livro.getTitulo().equals(titulo)) {

                livros.remove(livro);

                daoLivro.gravarArquivo();

                return "Livro deletado com sucesso";

            }

        }

        return "Livro não encontrado";

    }

}
