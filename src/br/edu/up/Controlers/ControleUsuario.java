package br.edu.up.Controlers;

import br.edu.up.Daos.GerenciadorDeUsuario;
import br.edu.up.Models.Usuario;

import java.util.ArrayList;
import java.util.List;

public class ControleUsuario {

    private List<Usuario> usuarios = new ArrayList<>();

    private GerenciadorDeUsuario daoUsuario = new GerenciadorDeUsuario();

    public String addUsuario(Usuario usuario){

        for (Usuario usuarioVeri : usuarios) {

            if (usuarioVeri.getCpf().equals(usuario.getCpf())) {
                return "Cpf já cadastrado";
            }

        }

        usuarios.add(usuario);
        daoUsuario.gravarArquivo();
        return "Usuario cadastrado com sucesso";

    }

    public ControleUsuario() {

        usuarios = daoUsuario.getUsuarios();

    }

    public String listarUsuario(){

        StringBuilder listarUsuario = new StringBuilder();

        if (usuarios.isEmpty()) {
            return "Nenhum usuario cadastrado";
        }

        for (Usuario usuario : usuarios) {
            
            listarUsuario.append("Nome: ")
                         .append(usuario.getNome())
                         .append(", CPF: ")
                         .append(usuario.getCpf())
                         .append(", Telefone: ")
                         .append(usuario.getTelefone())
                         .append(", Email: ")
                         .append(usuario.getEmail())
                         .append(", Endereço: ")
                         .append(usuario.getEndereco())
                         .append("\n");

        }

        return listarUsuario.toString();

    }

    public String listarUsuarioCpf(){

        StringBuilder listarUsuario = new StringBuilder();

        if (usuarios.isEmpty()) {
            return "Nenhum usuario cadastrado";
        }

        for (Usuario usuario : usuarios) {
            
            listarUsuario.append("Nome: ")
                         .append(usuario.getNome())
                         .append(", CPF: ")
                         .append(usuario.getCpf())
                         .append("\n");

        }

        return listarUsuario.toString();

    }

    public String mostrarNome(String cpf){

        for (Usuario usuario : usuarios) {

            if (usuario.getCpf().equals(cpf)){
                return usuario.getNome();  
            }

        }

        return "";

    }

    public String DeletarUsuario(String cpf){

        if (usuarios.isEmpty()){
            return "Nenhum usuario cadastrado";
        }

        for (Usuario usuario : usuarios) {

            if (usuario.getCpf().equals(cpf)) {
                
                usuarios.remove(usuario);

                daoUsuario.gravarArquivo();

                return "Usuario deletado com sucesso";

            }

        }

        return "Usuario não encontrado";

    }

}