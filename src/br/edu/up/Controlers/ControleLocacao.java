package br.edu.up.Controlers;

import br.edu.up.Daos.GerenciadorDeLocacao;
import br.edu.up.Models.Locacao;
import java.util.ArrayList;
import java.util.List;

public class ControleLocacao{

    private GerenciadorDeLocacao daoLocacao = new GerenciadorDeLocacao();
    private ControleLivro controleLivro = new ControleLivro();
    private ControleUsuario controleUsuario = new ControleUsuario();
    private List<Locacao> locacoes = new ArrayList<Locacao>();
    private int maiorId = 0;

    public ControleLocacao() {

        locacoes = daoLocacao.getLocacoes();

        for (Locacao locacao : this.locacoes) {
            
            if (locacao.getLocacaoId() > maiorId) {
                maiorId = locacao.getLocacaoId();
            }

        }

    }

    public int getId() {
        return ++maiorId;
    }

    public String addLocacao(Locacao locacao){
        locacoes.add(locacao);
        daoLocacao.gravarArquivo();
        return "locacao efetuada com sucesso";
    }

    public Locacao getLocacaoPorId(int locacaoId) {
        for (Locacao locacao : locacoes) {
            if (locacao.getLocacaoId()==locacaoId) {
                return locacao;
            }
        }
        return null; 
    }

    public String LivroDevolver(int locacaoId) {

        if (locacoes.isEmpty()) {
            return "Nenhum locação cadastrado";
        }

        for (Locacao locacao : locacoes) {

            if (locacao.getLocacaoId()==locacaoId) {

                locacoes.remove(locacao);

                daoLocacao.gravarArquivo();

                return "Devolução efetuada com sucesso";

            }

        }

        return "locação não encontrada";
        
    }

    public String listarLocacoes(){

        StringBuilder listarLocacoes = new StringBuilder();

        if (locacoes.isEmpty()) {
            return "Nenhum locação cadastrado";
        }

        for (Locacao locacao : locacoes) {

            listarLocacoes.append("ID: ")
                          .append(locacao.getLocacaoId())
                          .append(", Livro: ")
                          .append(controleLivro.mostrarNome(locacao.getLivroId()))
                          .append(", Usuario: ")
                          .append(controleUsuario.mostrarNome(locacao.getCpf()))
                          .append("\n");

        }

        return listarLocacoes.toString();

    }

}