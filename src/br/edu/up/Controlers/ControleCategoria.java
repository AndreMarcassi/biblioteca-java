package br.edu.up.Controlers;

import br.edu.up.Daos.GerenciadorDeCategoria;
import br.edu.up.Models.Categoria;

import java.util.ArrayList;
import java.util.List;

public class ControleCategoria {

    private List<Categoria> categorias = new ArrayList<>();
    private int maiorId = 0;

    private GerenciadorDeCategoria daoCategoria = new GerenciadorDeCategoria();

    public ControleCategoria() {

        categorias = daoCategoria.getCategorias();

        for (Categoria categoria : this.categorias) {
            
            if (categoria.getCategoriaId() > maiorId) {
                maiorId = categoria.getCategoriaId();
            }

        }

    }

    public int getId() {
        return ++maiorId;
    }

    public String addCategoria(Categoria nomeCategoria){

        if (categorias.contains(nomeCategoria)) {
            return "Categoria já cadastrada";
        }else{
            categorias.add(nomeCategoria);
            daoCategoria.gravarArquivo();
            return "Categoria cadastrada com sucesso";
        }
    }

    public String mostrarUmaCategoria(int idCategoria){

        for (Categoria categoria : categorias) {

            if (categoria.getCategoriaId()==idCategoria) {
                return categoria.getNomeCategoria();
            }
            
        }

        return "";

    }


    public String listarCategorias(){

        StringBuilder listarCategorias = new StringBuilder();

        if (categorias.isEmpty()) {
            return "nenhuma categoria cadastrado";
        }

        for (Categoria categoria : categorias) {
            listarCategorias.append("ID: ")
                            .append(categoria.getCategoriaId())
                            .append(", categoria: ")
                            .append(categoria.getNomeCategoria())
                            .append("\n");
        }

        return listarCategorias.toString();

    }
    
}