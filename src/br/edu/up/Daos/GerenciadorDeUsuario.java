package br.edu.up.Daos;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import br.edu.up.Models.Usuario;

public class GerenciadorDeUsuario {

    private String header = "nome;cpf;telefone;email;endereco";

    private String nomeDoArquivo = "src/br/edu/up/Daos/csvs/usuarios.csv";
    List<Usuario> listaDeUsuarios = new ArrayList<>();

    public List<Usuario> getUsuarios() {

        try {

            File arquivoLeitura = new File(nomeDoArquivo);

            Scanner leitor = new Scanner(arquivoLeitura);

            if (leitor.hasNextLine()) {
                header = leitor.nextLine();
            }

            while (leitor.hasNextLine()) {
                String linha = leitor.nextLine();
                String[] dados = linha.split(";");
                
                String nome = dados[0];
                String cpf = dados[1];
                String telefone = dados[2];
                String email = dados[3];
                String endereco= dados[4];

                Usuario usuario = new Usuario (nome, cpf, telefone, email, endereco);
                listaDeUsuarios.add(usuario);
            }
            leitor.close();

        } catch(Exception e){
            System.out.println("Arquivo não encontrado!");
        }

        return listaDeUsuarios;

    }

    public void gravarArquivo() {

        try {

            FileWriter arquivoGravar = new FileWriter(nomeDoArquivo);
            PrintWriter gravador = new PrintWriter(arquivoGravar);
            gravador.println(header);

            for (Usuario usuario : listaDeUsuarios) {
                gravador.println(usuario.toCSV());
            }

            gravador.close();

        } catch (IOException e) {
            System.out.println("Não foi possível gravar o arquivo!");
        }
    }
    
}