package br.edu.up.Daos;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import br.edu.up.Models.Locacao;

public class GerenciadorDeLocacao {

    private String header = "locacaoId;usuarioId;livroId;dataLocacao;dataDevolucao";

    private String nomeDoArquivo = "src/br/edu/up/Daos/csvs/locacoes.csv";

    List<Locacao> listaDeLocacoes = new ArrayList<>();

    public List<Locacao> getLocacoes() {

        try {

            File arquivoLeitura = new File(nomeDoArquivo);

            Scanner leitor = new Scanner(arquivoLeitura);

            if (leitor.hasNextLine()) {
                header = leitor.nextLine();
            }

            while (leitor.hasNextLine()) {
                String linha = leitor.nextLine();
                String[] dados = linha.split(";");

                int locacaoId = Integer.parseInt(dados[0]);
                String usuarioId = dados[1];
                int livroId = Integer.parseInt(dados[2]);
                LocalDate dataLocacao = LocalDate.parse(dados[3]);
                LocalDate dataDevolucao = LocalDate.parse(dados[4]);

                Locacao locacao = new Locacao(locacaoId, usuarioId, livroId, dataLocacao, dataDevolucao);
                listaDeLocacoes.add(locacao);

            }
            leitor.close();

        } catch (Exception e) {
            System.out.println("Arquivo não encontrado!");
        }

        return listaDeLocacoes;

    }

    public void gravarArquivo() {

        try {

            FileWriter arquivoGravar = new FileWriter(nomeDoArquivo);
            PrintWriter gravador = new PrintWriter(arquivoGravar);
            gravador.println(header);

            for (Locacao locacao : listaDeLocacoes) {
                gravador.println(locacao.toCSV());
            }

            gravador.close();

        } catch (IOException e) {
            System.out.println("Não foi possível gravar o arquivo!");
        }
    }

}