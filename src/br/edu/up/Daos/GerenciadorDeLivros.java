package br.edu.up.Daos;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import br.edu.up.Models.Livro;

public class GerenciadorDeLivros {

    private String header = "livroID;titulo;editora;autor;quantidade;idCategoria";

    private String nomeDoArquivo = "src/br/edu/up/Daos/csvs/livros.csv";

    List<Livro> listaDeLivros = new ArrayList<>();

    public List<Livro> getLivros() {

        try {

            File arquivoLeitura = new File(nomeDoArquivo);

            Scanner leitor = new Scanner(arquivoLeitura);

            if (leitor.hasNextLine()) {
                header = leitor.nextLine();
            }

            while (leitor.hasNextLine()) {
                String linha = leitor.nextLine();
                String[] dados = linha.split(";");

                // livroID;titulo;editora;autor;quantidade;idCategoria

                int livroID = Integer.parseInt(dados[0]);
                String titulo = dados[1];
                String editora = dados[2];
                String autor = dados[3];
                int quantidade = Integer.parseInt(dados[4]);
                int idCategoria = Integer.parseInt(dados[5]);

                Livro livro = new Livro(livroID,titulo,editora,autor,quantidade,idCategoria);
                listaDeLivros.add(livro);
            }
            leitor.close();

        } catch(Exception e){
            System.out.println("Arquivo não encontrado!");
        }

        return  listaDeLivros;

    }

    public void gravarArquivo() {

        try {

            FileWriter arquivoGravar = new FileWriter(nomeDoArquivo);
            PrintWriter gravador = new PrintWriter(arquivoGravar);
            gravador.println(header);

            for (Livro livro : listaDeLivros) {
                gravador.println(livro.toCSV());
            }

            gravador.close();

        } catch (IOException e) {
            System.out.println("Não foi possível gravar o arquivo!");
        }
    }
    
}