package br.edu.up.Models;

import java.time.LocalDate;



public class Locacao {

    private int locacaoId;
    private String cpf;
    private int livroId;
    private LocalDate dataLocacao;
    private LocalDate dataDevolucao;

    public Locacao(){

    }

    public Locacao(int locacaoId, String cpf, int livroId, LocalDate dataLocacao, LocalDate dataDevolucao) {
        this.locacaoId = locacaoId;
        this.cpf = cpf;
        this.livroId = livroId;
        this.dataLocacao = dataLocacao;
        this.dataDevolucao = dataDevolucao; 
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public int getLivroId() {
        return livroId;
    }

    public void setLivroId(int livroId) {
        this.livroId = livroId;
    }

    public LocalDate getDataLocacao() {
        return dataLocacao;
    }

    public void setDataLocacao(LocalDate dataLocacao) {
        this.dataLocacao = dataLocacao;
    }

    public LocalDate getDataDevolucao() {
        return dataDevolucao;
    }

    public void setDataDevolucao(LocalDate dataDevolucao) {
        this.dataDevolucao = dataDevolucao;
    }

    public int getLocacaoId() {
        return locacaoId;
    }

    public void setLocacaoId(int locacaoId) {
        this.locacaoId = locacaoId;
    }

    public String toCSV(){
        return locacaoId + ";" + cpf + ";" + livroId + ";" + dataLocacao + ";" + dataDevolucao;
    }

}