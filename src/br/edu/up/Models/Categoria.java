package br.edu.up.Models;

import java.util.Objects;

public class Categoria {

    private int categoriaId;
    private String nomeCategoria;

    public Categoria(){
    }

    public Categoria(int categoriaId,String nomeCategoria) {
        this.categoriaId = categoriaId;
        this.nomeCategoria = nomeCategoria;
    }

    public String getNomeCategoria() {
        return nomeCategoria;
    }

    public void setNomeCategoria(String nomeCategoria) {
        this.nomeCategoria = nomeCategoria;
    }

    public int getCategoriaId() {
        return categoriaId;
    }

    public void setCategoriaId(int categoriaId) {
        this.categoriaId = categoriaId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Categoria categoria = (Categoria) o;
        return Objects.equals(nomeCategoria, categoria.nomeCategoria);
    }

    public String toCSV(){
        return categoriaId + ";" + nomeCategoria;
    }

}