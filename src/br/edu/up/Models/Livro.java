package br.edu.up.Models;

public class Livro {

    private int livroId;
    private String titulo;
    private String editora;
    private String autor;
    private int quantidade;
    private int idCategoria;

    public Livro() {
    }

    public Livro(int livroId, String titulo, String editora, String autor, int quantidade, int idCategoria) {
        this.livroId = livroId;
        this.titulo = titulo;
        this.editora = editora;
        this.autor = autor;
        this.quantidade = quantidade;
        this.idCategoria = idCategoria;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getEditora() {
        return editora;
    }

    public void setEditora(String editora) {
        this.editora = editora;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public int getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(int idCategoria) {
        this.idCategoria = idCategoria;
    }

    public int getLivroId() {
        return livroId;
    }

    public void setLivroId(int livroId) {
        this.livroId = livroId;
    }

    public String toCSV(){
        return livroId + ";" + titulo + ";" + editora + ";" + autor + ";" + quantidade + ";" + idCategoria;
    }
    
}